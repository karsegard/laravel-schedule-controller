<?php

namespace KDA\Scheduler;

use KDA\Laravel\PackageServiceProvider;

use Illuminate\Http\Request;

class ServiceProvider extends PackageServiceProvider
{

    
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasRoutes;
   
    protected $routes = [
        '/kda/scheduler.php'
    ];

    protected $configs = [
        'kda/scheduler.php'=> 'kda.scheduler'
    ];
    
    protected function packageBaseDir () {
        return dirname(__DIR__,1);
    }

 
}
