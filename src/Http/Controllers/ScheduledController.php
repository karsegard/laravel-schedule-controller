<?php

namespace KDA\Scheduler\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;

class ScheduledController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function index()
    {
      
        $artisan = \Artisan::call("schedule:run");
        $output = \Artisan::output();
        return $output;
    }
}
