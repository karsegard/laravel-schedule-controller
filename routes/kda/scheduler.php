<?php


use Illuminate\Support\Facades\Route;

Route::group(
    [
        'namespace'  => 'KDA\Scheduler\Http\Controllers',
        'middleware' => 'web',
        'prefix'     => '/scheduler',
    ],
    function () {
        Route::get('/', 'ScheduledController@index')->name('scheduler');
    }
);
